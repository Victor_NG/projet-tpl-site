<?php
    class LienDao {
    static function get($fichier){
        $data = json_decode(file_get_contents($fichier));
        $resultat = array();

            foreach($data as $d)
                    {
                        array_push($resultat, new Lien($d->lien,$d->name));
                    }

        return $resultat;
    }
    
    static function put($fichier, $lien, $name){
        $data = json_decode(file_get_contents($fichier));

            $unLien = new Lien($lien,$name);
            if($unLien->valider_estunlien())
            {
            array_push($data, $unLien);
            }
            file_put_contents($fichier, json_encode($data, JSON_PRETTY_PRINT|JSON_UNESCAPED_SLASHES));
        
        return $unLien;
    }
}

?>
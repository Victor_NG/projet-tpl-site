<?php
  include("modeles/lien.php");
  include("fonctions/index.php");
  include("parametres/constante.php");
  
    $categorie = basename($_GET["c"]);



    if($categorie == ""){
        header("location: /");
        die();
    }

    if(isset($_POST['link']) && isset($_POST['title'])){
        $title = $_POST["title"];
        $link = $_POST["link"];
        
        $lien = new Lien($link, $title);

        $data = json_decode(file_get_contents("./data/" . $categorie . ".json"));

        if ($lien->valider_estunlien())
      {
        
        array_push($data, $lien);
      }
        
        file_put_contents("./data/" . $categorie . ".json", json_encode($data, JSON_PRETTY_PRINT|JSON_UNESCAPED_SLASHES));
         header("Refresh:0");
        die();

    }
?>

<!DOCTYPE html>
<html lang="en">

  <?php 
       head(ucfirst($categorie)); ?> </title>
  <body>

    <!-- Header -->
    <div class="d-flex flex-column flex-md-row align-items-center p-3 px-md-4 mb-3 bg-white border-bottom shadow-sm">
      <h5 class="my-0 mr-md-auto font-weight-normal">Shitty Application</h5>
      </div>

        <!-- Search -->
        <?php    

      genereListe(ucfirst($categorie), "./data/" .$categorie . ".json");

      ?>

    <?php
    bouton();
    ?>

            

    <!-- Footer -->
    
    <?php foot(); ?>
   